;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; load in the libraries
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/wrf/WRF_contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"

;load "./load_variables.ncl"
;fils = systemfunc ("readlink -f ./load_variables.ncl")
;print(fils)
;load fils
load "/home/jhbravo/gitpr/wrfout_sig2pres/load_variables.ncl"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; start the primary wrfout_to_cf.ncl program
begin
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; configuration settings
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; set the units for time
  TimeUnits = "hours since 2001-01-01 00:00:00"
  ; set the values for pressure to be interpolated

  vert_coord = "pressure"
  if (vert_coord .eq. "pressure") then
    if (.not.isvar("p_lv")) then
      interp_levels = (/850, 700, 500, 250/)
      ; interp_levels = (/1000, 850, 700, 500, 300/)
    end if
    ;vert_coord       = "pressure"
    pressure         = interp_levels
    opts             = True
    opts@extrapolate = True
    opts@field_type  = "p"
    opts@logP        = True
  end if

  ; set default values for file_in, dir_in, and file_out, if not specified
  if (.not.isvar("file_in")) then
    file_in  = "/home/jhbravo/ingest/forecast_o/WRF_GFS0.25_badeba/2021072412/wrfout_d01_2021-07-24_12:00:00"
    strs1    = str_split(file_in, "/")
    strs2    = strs1(::-1)
    file_in_n  = strs2(0)
    strs3    = strs2(1:)
    strs4    = strs3(::-1)
    dir_in_n   = "/" + str_join(strs4, "/") + "/"
  end if
  if (.not.isvar("file_out")) then
    file_out_n = str_sub_str(file_in_n,"wrfout","wrfpost") + ".nc"
    file_out = dir_in_n  + file_out_n
  end if

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; open the input netcdf file (wrfout file)
  wrfout = addfile(file_in+".nc","r")
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; set the netcdf file global attributes
  fileAtt                = True
  fileAtt@creation_date  = systemfunc("date")
  fileAtt@institution    = "University of Colorado at Boulder - CIRES"
  fileAtt@created_by     = "Mark Seefeldt - mark.seefeldt@colorado.edu"
  fileAtt@notes          = "Created with NCL script: wrfout_to_cf.ncl v2.0"
  fileAtt@Conventions    = "CF 1.6, Standard Name Table v19"
  fileAtt@source         = file_in_n
  fileAtt@title          = file_out_n
  ;inspired by code: https://www.ncl.ucar.edu/Support/talk_archives/2014/1327.html
  oldAtt = getfileatts(wrfout)
  do i=0,dimsizes(oldAtt)-1
    fileAtt@$oldAtt(i)$ = wrfout@$oldAtt(i)$
  end do
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; time coordinate
  ; -the time in wrfout is in an odd character format
  TimeChar = wrfout->Times
  ; -determine the number of dimensions for time
  DimTimeChar = dimsizes(TimeChar)
  nTime = DimTimeChar(0)
  ; -convert the wrfout time to a CF compliant time
  ;  "hours since 1901-01-01 00:00:00"
  time_in = wrf_times_c(TimeChar, 1)
  ; -create an array indicating the year, month, day, hour, minute, second
  utc_date = floattoint(ut_calendar(time_in, 0))
  ; -create the final variable for time with the units selected
  time = (/ut_inv_calendar(utc_date(:,0), utc_date(:,1), utc_date(:,2),utc_date(:,3), utc_date(:,4), utc_date(:,5), TimeUnits, 0)/)  ;time
    time@long_name     = "Time"
    time@standard_name = "time"
    time@units         = TimeUnits
    time@calendar      = "standard"
    time!0             = "time"
    time&time          = time
  ; -convert the wrfout time to a DateTime integer for easy reading
  if (outDateTime) then
    DateTime = (/wrf_times_c(TimeChar, 3)/) ;time
      DateTime@long_name = "Date and Time"
      DateTime!0         = "time"
      DateTime&time      = time
  end if
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; vertical variables / coordinates
  ; Note:  pressure levels are assigned in the beginning section
  if (outPressure) then
    ;nPressure = dimsizes(pressure)       ;pressure vertical coordinate
    pressure@long_name       = "Pressure Levels"
      pressure@standard_name = "air_pressure"
      pressure@units         = "hPa"
      pressure@positive      = "down"
      pressure!0             = "pressure"
      pressure&pressure      = interp_levels
      nPressure = dimsizes(interp_levels) ;pressure vertical coordinate
  end if
  if (outEta .or. outPressure)
    eta = (/wrfout->ZNU(0,:)/)           ;eta values on half-levels (mass)
      eta@long_name     = "Eta Levels (mass points)"
      eta@standard_name = "atmosphere_sigma_coordinate"
      eta@units         = "1"
      eta@positive      = "down"
      eta@formula_terms = "sigma: eta ps: p_sfc ptop: p_top"
      eta!0             = "eta"
      eta&eta = eta
      nEta    = dimsizes(eta)
  end if
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; one-dimensional general model variables
  if (outPtop .or. outEta) then
    p_top_in = (/wrfout->P_TOP/)/100.    ;pressure at top of model
    ;in some files P_TOP has two dimensions, in some it has one dimension
    if ((dimsizes(dimsizes(p_top_in))) .eq. 2) then
      p_top = p_top_in(0,0)
    else
      p_top = p_top_in(0)
    end if
    p_top@long_name     = "Pressure at Top of the Model"
    p_top@standard_name = "air_pressure"
    p_top@units         = "hPa"
  end if
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; two-dimensional mapping variables
  lat = wrf_user_getvar(wrfout,"XLAT",0)  ;lat (mass)
    lat@long_name     = "Latitude"
    lat@standard_name = "latitude"
    lat@units         = "degrees_north"
    lat@Grib2_Parameter_Category = "Other"
    lat!0             = "south_north"
    lat!1             = "west_east"
    DimLat = dimsizes(lat)
    nS_N   = DimLat(0)           ;S_N dimension
    nW_E   = DimLat(1)           ;W_E dimension
  lon = wrf_user_getvar(wrfout,"XLONG",0) ;lon (mass)
    lon@long_name     = "Longitude"
    lon@standard_name = "longitude"
    lon@units         = "degrees_east"
    lon@Grib2_Parameter_Category = "Other"
    lon!0             = "south_north"
    lon!1             = "west_east"
  Z_sfc = wrf_user_getvar(wrfout,"HGT",0) ;Z_sfc
    Z_sfc@long_name     = "Terrain Height"
    Z_sfc@standard_name = "height"
    Z_sfc@units         = "m"
    Z_sfc@coordinates   = "lon lat"
    Z_sfc@Grib2_Parameter_Category = "Other"
    Z_sfc!0             = "south_north"
    Z_sfc!1             = "west_east"
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; one-dimensional coordinate system
  if (axis) then
    south_north = ispan(0,nS_N-1,1)      ;south_north
      south_north@long_name = "y-coordinate in Cartesian system"
      south_north@axis      = "Y"
      south_north@units     = "m"
      south_north!0         = "south_north"
    west_east = ispan(0,nW_E-1,1)        ;west_east
      west_east@long_name = "x-coordinate in Cartesian system"
      west_east@axis      = "X"
      west_east@units     = "m"
      west_east!0         = "west_east"
  end if
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; two-dimensional near-surface / surface met variables
  ;   -retrieved directly from the wrfout file - or
  ;   -derived/diagnostic fields using wrf_user_getvar
  if (out2dMet) then
    if (out2dMet@SST) then
      SST = (/wrfout->SST/)                                        ;SST
        SST@long_name     = "Sea-Surface Temperature"
        SST@standard_name = "sea_surface_temperature"
        SST@units         = "K"
        SST@Grib2_Parameter_Discipline    = "Oceanographic products"
        SST@Grib2_Parameter_Category      = "Temperature"
        SST@Grib2_Parameter_Name          = "Temperature"
        SST@Grib2_Generating_Process_Type = "Forecast"
        assignVarAttCoord(SST,time,0,0)
    end if
    if (out2dMet@T_sfc) then
      T_sfc = (/wrfout->TSK/)                                      ;T_sfc
        T_sfc@long_name    = "Temperature at the Surface"
        T_sfc@standard_name = "surface_temperature"
        T_sfc@units         = "K"
        T_sfc@Grib2_Parameter_Discipline    = "Meteorological products"
        T_sfc@Grib2_Parameter_Category      = "Temperature"
        T_sfc@Grib2_Parameter_Name          = "Temperature"
        T_sfc@Grib2_Generating_Process_Type = "Forecast"
        assignVarAttCoord(T_sfc,time,0,0)
    end if
    if (out2dMet@p_sfc) then
      p_sfc = (/wrfout->PSFC/)/100.                                ;p_sfc
        p_sfc@long_name     = "Pressure at the Surface"
        p_sfc@standard_name = "surface_air_pressure"
        p_sfc@units         = "hPa"
        p_sfc@Grib2_Parameter_Discipline    = "Meteorological products"
        p_sfc@Grib2_Parameter_Category      = "Mass"
        p_sfc@Grib2_Parameter_Name          = "Pressure"
        p_sfc@Grib2_Generating_Process_Type = "Forecast"
        assignVarAttCoord(p_sfc,time,0,0)
    end if
    if (out2dMet@slp) then                                         ;slp
      slp = (/wrf_user_getvar(wrfout,"slp",-1)/)
        slp@long_name     = "Sea-Level Pressure"
        slp@standard_name = "air_pressure_at_sea_level"
        slp@units         = "hPa"
        slp@Grib2_Parameter_Discipline    = "Meteorological products"
        slp@Grib2_Parameter_Category      = "Mass"
        slp@Grib2_Parameter_Name          = "Pressure"
        slp@Grib2_Generating_Process_Type = "Forecast"
        assignVarAttCoord(slp,time,0,0)
    end if
    if (out2dMet@T_2m) then
      T_2m = (/wrf_user_getvar(wrfout,"T2",-1)/)   ; T2 in Kelvin
        T_2m@long_name     = "Temperature at 2 m"
        T_2m@standard_name = "air_temperature"
        T_2m@units         = "K"
        T_2m@Grib2_Parameter_Discipline    = "Meteorological products"
        T_2m@Grib2_Parameter_Category      = "Temperature"
        T_2m@Grib2_Parameter_Name          = "Temperature"
        T_2m@Grib2_Generating_Process_Type = "Forecast"
        assignVarAttCoord(T_2m,time,0,0)
    end if
    if (out2dMet@ctt) then
      ctt = (/wrf_user_getvar(wrfout,"ctt",-1)/)
        ctt@long_name  = "Cloud Top Temperature"
        ctt@short_name = "CTT"
        ctt@units      = "degC"
        ctt@standard_name = "cloud_top_temperature"
        ctt@Grib2_Parameter_Discipline    = "Meteorological products"
        ctt@Grib2_Parameter_Category      = "Cloud"
        ctt@Grib2_Parameter_Name          = "Cloud Top Temperature"
        ctt@Grib2_Generating_Process_Type = "Forecast"
        assignVarAttCoord(ctt,time,0,0)
    end if
    if (out2dMet@cfrac) then
      cfrac1 = (/wrf_user_getvar(wrfout,"cfrac",-1)/)
        cfrac = cfrac1(0,:,:,:)
        cfrac@long_name  = "Cloud fraction"
        cfrac@short_name = "cfrac"
        cfrac@units      = "%"
        cfrac@standard_name = "cloud_fraction"
        cfrac@Grib2_Parameter_Discipline    = "Meteorological products"
        cfrac@Grib2_Parameter_Category      = "Cloud"
        cfrac@Grib2_Parameter_Name          = "Cloud fraction"
        cfrac@Grib2_Generating_Process_Type = "Forecast"
        assignVarAttCoord(cfrac,time,0,0)
    end if
  end if
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; three-dimensional upper-level (eta) metorology variables
  ;   -retrieved directly from the wrfout file - or
  ;   -derived/diagnostic fields using wrf_user_getvar
  if (outEta .or. outPressure)
    if (outEta@T_e .or. outPressure@T_p) then
      T_e = (/wrf_user_getvar(wrfout,"tk",-1)/)
        T_e@standard_name = "air_temperature"
        T_e@units         = "K"
        T_e@Grib2_Parameter_Discipline    = "Meteorological products"
        T_e@Grib2_Parameter_Category      = "Temperature"
        T_e@Grib2_Parameter_Name          = "Temperature"
        T_e@Grib2_Generating_Process_Type = "Forecast"
        if (outEta@T_e) then
          T_e@long_name     = "Temperature @ eta_levels"
          assignVarAttCoord(T_e,time,eta,2)
        end if
        if (outPressure@T_p) then
          T_p = wrf_user_vert_interp(wrfout,T_e,vert_coord,interp_levels,opts)
          T_p@long_name     = "Temperature @ pressure_levels"
          assignVarAttCoord(T_p,time,interp_levels,1)
          copy_VarAtts(T_e, T_p)
        end if
    end if
    ;;;;;;;;;;;;;;;;;;;;;;;;;;
    if (outEta@Z_e .or. outPressure@Z_p) then
      Z_e = (/wrf_user_getvar(wrfout,"z",-1)/)
        Z_e@standard_name = "geopotential_height"
        Z_e@units         = "m"
        Z_e@Grib2_Parameter_Discipline    = "Meteorological products"
        Z_e@Grib2_Parameter_Category      = "Mass"
        Z_e@Grib2_Parameter_Name          = "Geopotential height"
        Z_e@Grib2_Generating_Process_Type = "Forecast"
        if (outEta@Z_e) then
          Z_e@long_name     = "Geopotential Height @ eta_levels"
          assignVarAttCoord(Z_e,time,eta,2)
        end if
        if (outPressure@T_p) then
          Z_p = wrf_user_vert_interp(wrfout,Z_e,vert_coord,interp_levels,opts)
          Z_p@long_name     = "Geopotential Height @ pressure_levels"
          assignVarAttCoord(Z_p,time,interp_levels,1)
          copy_VarAtts(Z_e, Z_p)
        end if
    end if
    ;;;;;;;;;;;;;;;;;;;;;;;;;;
    if (outEta@theta_e .or. outPressure@theta_p) then
      theta_e = (/wrf_user_getvar(wrfout,"th",-1)/)
        theta_e@standard_name = "air_potential_temperature"
        theta_e@units         = "K"
        theta_e@Grib2_Parameter_Discipline    = "Meteorological products"
        theta_e@Grib2_Parameter_Category      = "Temperature"
        theta_e@Grib2_Parameter_Name          = "Potential Temperature"
        theta_e@Grib2_Generating_Process_Type = "Forecast"
        if (outEta@theta_e) then
          theta_e@long_name     = "Potential Temperature @ eta_levels"
          assignVarAttCoord(theta_e,time,eta,2)
        end if
        if (outPressure@theta_p) then
          theta_p = wrf_user_vert_interp(wrfout,theta_e,vert_coord,interp_levels,opts)
          theta_p@long_name     = "Potential Temperature @ pressure_levels"
          assignVarAttCoord(theta_p,time,interp_levels,1)
          copy_VarAtts(theta_e, theta_p)
        end if
    end if
    ;;;;;;;;;;;;;;;;;;;;;;;;;;
    if (outEta@Td_e .or. outPressure@Td_p) then
      Td_e = (/wrf_user_getvar(wrfout,"td",-1)/)
        Td_e@standard_name = "dew_point_temperature"
        Td_e@units         = "degC"
        Td_e@Grib2_Parameter_Discipline    = "Meteorological products"
        Td_e@Grib2_Parameter_Category      = "Temperature"
        Td_e@Grib2_Parameter_Name          = "Dewpoint Temperature"
        Td_e@Grib2_Generating_Process_Type = "Forecast"
        if (outEta@Td_e) then
          Td_e@long_name     = "Dewpoint Temperature @ eta_levels"
          assignVarAttCoord(Td_e,time,eta,2)
        end if        
        if (outPressure@Td_p) then
          Td_p = wrf_user_vert_interp(wrfout,Td_e,vert_coord,interp_levels,opts)
          Td_p@long_name     = "Dewpoint Temperature @ pressure_levels"
          assignVarAttCoord(Td_p,time,interp_levels,1)
          copy_VarAtts(Td_e, Td_p)
        end if        
    end if

  end if
  ;;;;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;;;; remove the time attribute for all eta level variables ;;;;;;;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; -for some reason the time attribute is added to the eta levels during
  ;  the interpolation routines - delete these attributes
  ; if (isvar("pressure") .and. isatt(pressure, "pressure")) then
  ;   delete(pressure@pressure)
  ; end if
  ; if (isvar("T_e") .and. isatt(T_e, "time")) then
  ;   delete(T_e@time)
  ; end if
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;create filename and open post-processed netCDF file
  if isfilepresent(file_out) then
    system ("rm "+file_out)             ;remove any pre-exisiting file
  end if
  wrfpost = addfile(file_out,"c")    ;create new netCDF file
  filedimdef (wrfpost, "time", nTime, True)
  ; establish a variable for a new line in the attributes
  nl = integertochar(10)  ; newline character
  ; create the global attributes
  fileattdef(wrfpost, fileAtt)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;;;; write post-processed WRF data to netCDF file ;;;;;;;;;;;;;;;;;
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  ; check to see if the output file is to follow CMIP guidelines
;  if (outCMIP .ne. True) then
    ;use standard wrfout_to_cf variable names in the wrfpost output file
    ; -date and time variables
    wrfpost->time=time
    if (outDateTime) then
      wrfpost->DateTime=DateTime
    end if
    ; -vertical coordinate variables
    if (outPressure) then
      wrfpost->pressure=pressure
    end if
    if (outEta) then
      wrfpost->eta=eta
    end if
    ; -one-dimensional general model variables
    if (outPtop .or. outEta) then
      wrfpost->p_top=p_top
    end if
    ; -two-dimensional mapping variables
    wrfpost->lat=lat
    wrfpost->lon=lon
    wrfpost->Z_sfc=Z_sfc
    ; -two-dimensional near-surface / surface met variables
    if (out2dMet) then
      if (isvar("SST")) then
        wrfpost->SST=SST
      end if
      if (isvar("T_sfc")) then
        wrfpost->T_sfc=T_sfc
      end if
      if (isvar("p_sfc")) then
        wrfpost->p_sfc=p_sfc
      end if
      if (isvar("slp") .and. isvar("slp_b")) then
        wrfpost->slp=slp
        wrfpost->slp_b=slp_b
      else
        if (isvar("slp")) then
          wrfpost->slp=slp
        end if
        if (isvar("slp_b")) then
          wrfpost->slp=slp_b
        end if
      end if
      if (isvar("T_2m")) then
        wrfpost->T_2m=T_2m
      end if
      if (isvar("ctt")) then
        wrfpost->ctt=ctt
      end if
      if (isvar("cfrac")) then
        wrfpost->cfrac=cfrac
      end if
      if (isvar("theta_2m")) then
        wrfpost->theta_2m=theta_2m
      end if
      if (isvar("Td_2m")) then
        wrfpost->Td_2m=Td_2m
      end if
      if (isvar("r_v_2m")) then
        wrfpost->r_v_2m=r_v_2m
      end if
      if (isvar("q_2m")) then
        wrfpost->q_2m=q_2m
      end if
      if (isvar("rh_2m")) then
        wrfpost->rh_2m=rh_2m
      end if
      if (isvar("u_10m_gr")) then
        wrfpost->u_10m_gr=u_10m_gr
      end if
      if (isvar("v_10m_gr")) then
        wrfpost->v_10m_gr=v_10m_gr
      end if
      if (isvar("u_10m_tr")) then
        wrfpost->u_10m_tr=u_10m_tr
      end if
      if (isvar("v_10m_tr")) then
        wrfpost->v_10m_tr=v_10m_tr
      end if
      if (isvar("ws_10m")) then
        wrfpost->ws_10m=ws_10m
      end if
      if (isvar("wd_10m")) then
        wrfpost->wd_10m=wd_10m
      end if
      if (isvar("precip_g")) then
        wrfpost->precip_g=precip_g
      end if
      if (isvar("precip_c")) then
        wrfpost->precip_c=precip_c
      end if
      if (isvar("precip_fr")) then
        wrfpost->precip_fr=precip_fr
      end if
      if (isvar("dryairmass")) then
        wrfpost->dryairmass=dryairmass
      end if
      if (isvar("pblh")) then
        wrfpost->pblh=pblh
      end if
      if (isvar("rho")) then
        wrfpost->rho=rho
      end if
      ;new vars 2D
      if (isvar("precip_total")) then
        wrfpost->precip_total=precip_total
      end if
      if (isvar("precip_tendency")) then
        wrfpost->precip_tendency=precip_tendency
      end if
      if (isvar("mdbz")) then
        wrfpost->mdbz=mdbz
      end if
      if (isvar("updraft_helicity")) then
        wrfpost->updraft_helicity=updraft_helicity
      end if
      if (isvar("pw")) then
        wrfpost->pw=pw
      end if
      if (isvar("div_10m_tr")) then
        wrfpost->div_10m_tr=div_10m_tr
      end if
    end if
    ; -three-dimensional upper-level (eta) meteorology variables
    if (outEta) then
      if (isvar("T_e") .and. (outEta@T_e)) then
        wrfpost->T_e=T_e
      end if
      if (isvar("Z_e") .and. (outEta@Z_e)) then
        wrfpost->Z_e=Z_e
      end if
      if (isvar("theta_e") .and. (outEta@theta_e)) then
        wrfpost->theta_e=theta_e
      end if
      if (isvar("Td_e") .and. (outEta@Td_e)) then
        wrfpost->Td_e=Td_e
      end if
    end if
    ; -three-dimensional upper-level (pressure) meteorology variables
    if (outPressure) then
      if (isvar("T_p") .and. (outPressure@T_p)) then
        wrfpost->T_p=T_p
      end if
      if (isvar("Z_p") .and. (outPressure@Z_p)) then
        wrfpost->Z_p=Z_p
      end if
      if (isvar("theta_p") .and. (outPressure@theta_p)) then
        wrfpost->theta_p=theta_p
      end if
      if (isvar("Td_p") .and. (outPressure@Td_p)) then
        wrfpost->Td_p=Td_p
      end if
    end if
;  end if
;  ; check to see if the output file is to follow CMIP guidelines
;  if (outCMIP .eq. True) then
;
;  end if
end