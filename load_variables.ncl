;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; procedure to process the attributes in creating CF compliant WRF output
procedure assignVarAttCoord(x:numeric, time[*]:numeric, vert[*]:numeric, fl_vert:numeric)
begin
  ; assign the default missing value
  MissingValue               = 1e20
  ; set time for all variables
  x!0 = "time"
  x&time = time
  ; set the vertical coordinate depending on fl_vert
  if (fl_vert .eq. 1) then             ;pressure as vertical coordinate
    x!1 = "pressure"
    x&pressure = vert
    x@_FillValue = MissingValue
  end if
  if (fl_vert .eq. 2) then             ;eta as vertical coordinate
    x!1 = "eta"
    x&eta = vert
    x@_FillValue = MissingValue
  end if
  if (fl_vert .eq. 3) then             ;soil as vertical coordinate
    x!1 = "soil"
    x&soil = vert
    x@_FillValue = MissingValue
  end if
  ; set the horizontal coordinates
  if (fl_vert .eq. 0) then             ;no vertical coordinate
    x!1 = "south_north"
    x!2 = "west_east"
    x@_FillValue = MissingValue
  else                                 ;with vertical coordinate
    x!2 = "south_north"
    x!3 = "west_east"
    x@_FillValue = MissingValue
  end if
  ; set the mapping coordinates
  x@coordinates = "time lon lat"
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; set the flags for selecting variables to be included ;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Note:  Not all of the indicated values below can be extracted /
;        converted from a given wrfout file.  Some of the fields are
;        included with only specific WRF physics options.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; output settings
axis                       = True      ;one-dimensional coordinate fields
projection                 = False     ;CF projection info with fields
outPtop                    = False     ;include Ptop in the output file
;outCMIP                    = False     ;use CMIP variable names in output
;MissingValue               = -999999   ;missing value
; Note: MissingValue is currently assigned in the procedure assignVarAttCoord
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; time / date variables
outDateTime                = True      ;include a yyyymmddhh field
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; two-dimensional near-surface / surface met variables
out2dMet                   = True
out2dMet@SST               = True      ;sea-surface temperature                ;Sin Modificar
out2dMet@T_sfc             = True      ;temperature at the surface             ;Temperature_surface
out2dMet@p_sfc             = True      ;pressure at the surface                ;PSFC
out2dMet@slp               = True      ;sea-level pressure - using WRF-NCL     ;Sin Modificar
out2dMet@slp_b             = False      ;sea-level pressure - lowest eta level ;Sin Modificar
out2dMet@T_2m              = True      ;temperature at 2m                      ;T2
out2dMet@theta_2m          = False      ;potential temperature at 2m           ;theta2
out2dMet@Td_2m             = False      ;dewpoint temperature at 2m             ;td2
out2dMet@r_v_2m            = False      ;mixing ratio at 2m                     ;RH2
out2dMet@q_2m              = False      ;specific humidity at 2m
out2dMet@rh_2m             = False      ;relative humidity at 2m
out2dMet@u_10m_gr          = False      ;u wind - grid - at 10m
out2dMet@v_10m_gr          = False      ;v wind - grid - at 10m
out2dMet@u_10m_tr          = False      ;u wind - rotated to earth - at 10m
out2dMet@v_10m_tr          = False      ;v wind - rotated to earth - at 10m
out2dMet@ws_10m            = False      ;wind speed - at 10m wspd
out2dMet@wd_10m            = False      ;wind direction - earth - at 10m wspd
out2dMet@ctt               = True      ;Cloud Top Temperature
out2dMet@cfrac             = True      ;Cloud fraction
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; three-dimensional upper-level (eta levels) meteorology variables
outEta                     = False
outEta@p_e                 = False      ;pressure - eta
outEta@T_e                 = False      ;temperature - eta
outEta@Z_e                 = False      ;geopotential height - eta
outEta@theta_e             = False      ;potential temperature - eta
outEta@Td_e                = False      ;dewpoint temperature - eta
outEta@r_v_e               = False      ;mixing ratio - eta
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; three-dimensional upper-level (pressure levels) meteorology variables
outPressure                = True
outPressure@T_p            = True      ;temperature - pressure
outPressure@Z_p            = True      ;geopotential height - pressure
outPressure@theta_p        = True      ;potential temperature - pressure
outPressure@Td_p           = True      ;dewpoint temperature - pressure
outPressure@r_v_p          = True      ;mixing ratio - pressure